#include "Mesh.hpp"
#include <assimp/cimport.h>
#include <iostream>
#include <sstream>

MeshPtr makeSphere(float radius, unsigned int N)
{
    unsigned int M = N / 2;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (unsigned int i = 0; i < M; i++)
    {
        float theta = (float)glm::pi<float>() * i / M;
        float theta1 = (float)glm::pi<float>() * (i + 1) / M;

        for (unsigned int j = 0; j < N; j++)
        {
            float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
            float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

            //Первый треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

            //Второй треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}

glm::vec3 Torus::getCoordinates(double phi, double psi){
    double x = (R + r * cos(phi)) * cos(psi);
    double y = (R + r * cos(phi)) * sin(psi);
    double z = r * sin(phi);
    return glm::vec3(x, y, z);
}

std::pair<double, double> Torus::getDerX(double phi, double psi) {
    double x_phi = -r * sin(phi) * cos(psi);
    double x_psi = -(R + r * cos(phi)) * sin(psi);
    return std::make_pair(x_phi, x_psi);
}

std::pair<double, double> Torus::getDerY(double phi, double psi) {
    double y_phi = -r * sin(phi) * sin(psi);
    double y_psi = (R + r * cos(phi)) * cos(psi);
    return std::make_pair(y_phi, y_psi);
}

std::pair<double, double> Torus::getDerZ(double phi, double psi) {
    double z_phi = r * cos(phi);
    return std::make_pair(z_phi, 0);
}


glm::vec3 Torus::getNormalVector(double phi, double psi){
    std::pair<double, double> dx = getDerX(phi, psi);
    std::pair<double, double> dy = getDerY(phi, psi);
    std::pair<double, double> dz = getDerZ(phi, psi);
    double n_x = dy.first * dz.second - dz.first * dy.second;
    double n_y = dz.first * dx.second - dx.first * dz.second;
    double n_z = dx.first * dy.second - dx.second * dy.first;
    return glm::normalize(glm::vec3(n_x, n_y, n_z));
};

MeshPtr Torus::makeTorus(double R = 1, double r = 0.2, int segments_number = 50){
    this->R = R;
    this->r = r;
    this->segments_number = segments_number;

    this->lr[0] = 1.5;
    this->lr[1] = 2;
    this->lr[2] = 1;
    this->phi[0] = 0;
    this->phi[1] = glm::pi<float>();
    this->phi[2] = 0;
    this->theta[0] = glm::pi<float>() / 6;
    this->theta[1] = glm::pi<float>() / 4;
    this->theta[2] = 0;

    std::vector<glm::vec3> points;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (int phi_segment_idx = 0; phi_segment_idx < segments_number; phi_segment_idx++){
        double phi_1 = phi_segment_idx * 2 * glm::pi<float>() / segments_number;
        double phi_2 = (phi_segment_idx + 1) * 2 * glm::pi<float>() / segments_number;
        for (int psi_segment_idx = 0; psi_segment_idx < segments_number; psi_segment_idx++){
            double psi_1 = -glm::pi<float>() + psi_segment_idx * 2 * glm::pi<float>() / segments_number;
            double psi_2 = -glm::pi<float>() + (psi_segment_idx + 1) * 2 * glm::pi<float>() / segments_number;

            points.push_back(getCoordinates( phi_1, psi_1 ));
            points.push_back(getCoordinates( phi_1, psi_2 ));
            points.push_back(getCoordinates( phi_2, psi_1 ));

            points.push_back(getCoordinates(phi_2, psi_2));
            points.push_back(getCoordinates(phi_1, psi_2));
            points.push_back(getCoordinates(phi_2, psi_1));

            normals.push_back(getNormalVector(phi_1, psi_1));
            normals.push_back(getNormalVector(phi_1, psi_2));
            normals.push_back(getNormalVector(phi_2, psi_1));

            normals.push_back(getNormalVector(phi_2, psi_2));
            normals.push_back(getNormalVector(phi_1, psi_2));
            normals.push_back(getNormalVector(phi_2, psi_1));

            texcoords.push_back(glm::vec2(phi_1, psi_1));
            texcoords.push_back(glm::vec2(phi_1, psi_2));
            texcoords.push_back(glm::vec2(phi_2, psi_1));

            texcoords.push_back(glm::vec2(phi_2, psi_2));
            texcoords.push_back(glm::vec2(phi_1, psi_2));
            texcoords.push_back(glm::vec2(phi_2, psi_1));
        }
    }
    DataBufferPtr points_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    points_buf->setData(3 * sizeof(float) * points.size(), points.data());

    DataBufferPtr normals_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normals_buf->setData(3 * sizeof(float) * normals.size(), normals.data());

    DataBufferPtr tex_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    tex_buf->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, points_buf);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normals_buf);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, tex_buf);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(points.size());

    return mesh;
}
void Torus::makeScene()
{
    Application::makeScene();
    _cameraMover = std::make_shared<FreeCameraMover>();
    this->torus = makeTorus();
    this->shader = std::make_shared<ShaderProgram>("491ShenshinaData/texture.vert", "491ShenshinaData/texture.frag");

    this->marker = makeSphere(0.1f, 100);
    this->marker_shader = std::make_shared<ShaderProgram>("491ShenshinaData/marker.vert", "491ShenshinaData/marker.frag");

    this->light[0].position = glm::vec3(glm::cos(this->phi[0]) * glm::cos(this->theta[0]), glm::sin(this->phi[0]) * glm::cos(this->theta[0]), glm::sin(this->theta[0])) * this->lr[0];
    this->light[0].ambient = glm::vec3(0.2, 0.2, 0.2);
    this->light[0].diffuse = glm::vec3(0.9, 0., 0.);
    this->light[0].specular = glm::vec3(0.3, 0., 0.);

    this->light[1].position = glm::vec3(glm::cos(this->phi[1]) * glm::cos(this->theta[1]), glm::sin(this->phi[1]) * glm::cos(this->theta[1]), glm::sin(this->theta[1])) * this->lr[1];
    this->light[1].ambient = glm::vec3(0.2, 0.2, 0.2);
    this->light[1].diffuse = glm::vec3(0., 0.9, 0.);
    this->light[1].specular = glm::vec3(0., 0.3, 0.);

    this->light[2].position = glm::vec3(0, 0., 1.) * this->lr[2];
    this->light[2].ambient = glm::vec3(0.2, 0.2, 0.2);
    this->light[2].diffuse = glm::vec3(0., 0., 0.9);
    this->light[2].specular = glm::vec3(0., 0., 0.3);



    this->texture = loadTexture("491ShenshinaData/texture.jpg");

    glGenSamplers(1, &this->sampler);
    glSamplerParameteri(this->sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(this->sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glSamplerParameteri(this->sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(this->sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void Torus::update()
{
    double dt = glfwGetTime() - _oldTime;
    if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        this->segments_number = std::max(3, int(this->segments_number / 1.05 / sqrt(dt + 1)));
    }
    if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        this->segments_number = std::min(200, int(ceil(this->segments_number * 1.05 * sqrt(dt + 1))));
    }
    this->torus = makeTorus(this->R, this->r, static_cast<int>(this->segments_number));
    Application::update();
}

void Torus::updateGUI()
{
    Application::updateGUI();

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {

        if (ImGui::CollapsingHeader("Light"))
        {
            ImGui::ColorEdit3("ambient", glm::value_ptr(this->light[0].ambient));
            ImGui::ColorEdit3("diffuse", glm::value_ptr(this->light[0].diffuse));
            ImGui::ColorEdit3("specular", glm::value_ptr(this->light[0].specular));

            ImGui::SliderFloat("radius", &this->lr[0], 0.1f, 10.0f);
            ImGui::SliderFloat("phi", &this->phi[0], 0.0f, 2.0f * glm::pi<float>());
            ImGui::SliderFloat("theta", &this->theta[0], 0.0f, glm::pi<float>());
        }

    }
    ImGui::End();
}

void Torus::draw()
{
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    this->shader->use();

    this->shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    this->shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    this->light[0].position = glm::vec3(glm::cos(this->phi[0]) * glm::cos(this->theta[0]), glm::sin(this->phi[0]) * glm::cos(this->theta[0]), glm::sin(this->theta[0])) * (float)this->lr[0];

    std::ostringstream str;
    for (unsigned int i = 0; i < this->LightsNumber; i++)
    {
        str << "light[" << i << ']';

        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(this->light[i].position, 1.0f));

        this->shader->setVec3Uniform(str.str() + ".pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        this->shader->setVec3Uniform(str.str() + ".La", this->light[i].ambient);
        this->shader->setVec3Uniform(str.str() + ".Ld", this->light[i].diffuse);
        this->shader->setVec3Uniform(str.str() + ".Ls", this->light[i].specular);
        str.str("");
    }

    glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
    glBindSampler(0, this->sampler);
    this->texture->bind();

    this->shader->setIntUniform("diffuseTex", 0);


    {
        this->shader->setMat4Uniform("modelMatrix", this->torus->modelMatrix());
        this->shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * this->torus->modelMatrix()))));
        this->torus->draw();
    }

    for (unsigned int i = 0; i < this->LightsNumber - 1; i++)
    {
        this->marker_shader->use();
        this->marker_shader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), this->light[i].position));
        this->marker_shader->setVec4Uniform("color", glm::vec4(this->light[i].diffuse, 1.0f));
        this->marker->draw();
    }

    glBindSampler(0, 0);
    glUseProgram(0);
}