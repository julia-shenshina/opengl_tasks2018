/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};

uniform sampler2D diffuseTex;
uniform LightInfo light[3];

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;
const float cos = 0.95;
vec3 color = vec3(0.,0.,0.);

vec3 add_light(vec3 baseColor, LightInfo light, bool isSpotLight) {
	vec3 viewDirection = normalize(-posCamSpace.xyz);
	vec3 lightDirCamSpace;
	if (!isSpotLight){
	    lightDirCamSpace = normalize(light.pos - posCamSpace.xyz);
	}
	else{
	    lightDirCamSpace = normalize(-posCamSpace.xyz);
	}
	vec3 normal = -normalize(normalCamSpace);

	float distance = length(light.pos - posCamSpace.xyz);
    float attenuation = 2.0 / (1.0 + 2.0 * distance);

	vec3 color = vec3(0.,0.,0.);

    float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

    color += attenuation * baseColor * (light.La + light.Ld * NdotL);

    if (NdotL > 0.0)
    {
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света
        float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
        color += attenuation * light.Ls * Ks * blinnTerm;
    }

	return color;
}



void main()
{
    vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
    for (int i = 0; i < 3; i++){
        if (i < 2){
            color += add_light(diffuseColor, light[i], false); // обычные источники света
        }
        else{
            float current_cos = -dot(normalize(posCamSpace.xyz), vec3(0., 0., 1.));
            if(abs(current_cos) > cos) {
                color += add_light(diffuseColor, light[i], true); //конический источник, закреплённый на камере
            }
        }
    }
    fragColor = vec4(color, 1.0);
}