#version 330

uniform mat4 mvpMatrix; //произведение матриц projectionMatrix * viewMatrix * modelMatrix

layout(location = 0) in vec3 vertexPosition;

void main()
{
	gl_Position = mvpMatrix * vec4(vertexPosition, 1.0);
}