set(SRC_FILES
        Main.cpp
        common/Camera.cpp
        common/Camera.hpp
        common/Mesh.cpp
        common/Mesh.hpp
        common/ShaderProgram.cpp
        common/ShaderProgram.hpp
        common/Application.cpp
        common/Application.hpp
        common/DebugOutput.cpp
        common/DebugOutput.h
        common/Texture.cpp
        common/Texture.hpp
        common/LightInfo.hpp
)
include_directories(
        common
)

MAKE_TASK(491Shenshina 3 "${SRC_FILES}")

