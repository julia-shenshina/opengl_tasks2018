#include "Mesh.hpp"
#include <assimp/cimport.h>
#include <iostream>


glm::vec3 Torus::getCoordinates(double phi, double psi){
    double x = (R + r * cos(phi)) * cos(psi);
    double y = (R + r * cos(phi)) * sin(psi);
    double z = r * sin(phi);
    return glm::vec3(x, y, z);
}

std::pair<double, double> Torus::getDerX(double phi, double psi) {
    double x_phi = -r * sin(phi) * cos(psi);
    double x_psi = -(R + r * cos(phi)) * sin(psi);
    return std::make_pair(x_phi, x_psi);
}

std::pair<double, double> Torus::getDerY(double phi, double psi) {
    double y_phi = -r * sin(phi) * sin(psi);
    double y_psi = (R + r * cos(phi)) * cos(psi);
    return std::make_pair(y_phi, y_psi);
}

std::pair<double, double> Torus::getDerZ(double phi, double psi) {
    double z_phi = r * cos(phi);
    return std::make_pair(z_phi, 0);
}


glm::vec3 Torus::getNormalVector(double phi, double psi){
    std::pair<double, double> dx = getDerX(phi, psi);
    std::pair<double, double> dy = getDerY(phi, psi);
    std::pair<double, double> dz = getDerZ(phi, psi);
    double n_x = dy.first * dz.second - dz.first * dy.second;
    double n_y = dz.first * dx.second - dx.first * dz.second;
    double n_z = dx.first * dy.second - dx.second * dy.first;
    return glm::normalize(glm::vec3(n_x, n_y, n_z));
};

MeshPtr Torus::makeTorus(double R = 1, double r = 0.2, int segments_number = 20){
    this->R = R;
    this->r = r;
    this->segments_number = segments_number;
    std::vector<glm::vec3> points;
    std::vector<glm::vec3> normals;

    for (int phi_segment_idx = 0; phi_segment_idx < segments_number; phi_segment_idx++){
        double phi_1 = phi_segment_idx * 2 * glm::pi<float>() / segments_number;
        double phi_2 = (phi_segment_idx + 1) * 2 * glm::pi<float>() / segments_number;
        for (int psi_segment_idx = 0; psi_segment_idx < segments_number; psi_segment_idx++){
            double psi_1 = -glm::pi<float>() + psi_segment_idx * 2 * glm::pi<float>() / segments_number;
            double psi_2 = -glm::pi<float>() + (psi_segment_idx + 1) * 2 * glm::pi<float>() / segments_number;

            points.push_back(getCoordinates( phi_1, psi_1 ));
            points.push_back(getCoordinates( phi_1, psi_2 ));
            points.push_back(getCoordinates( phi_2, psi_1 ));

            points.push_back(getCoordinates(phi_2, psi_2));
            points.push_back(getCoordinates(phi_1, psi_2));
            points.push_back(getCoordinates(phi_2, psi_1));

            normals.push_back(getNormalVector(phi_1, psi_1));
            normals.push_back(getNormalVector(phi_1, psi_2));
            normals.push_back(getNormalVector(phi_2, psi_1));

            normals.push_back(getNormalVector(phi_2, psi_2));
            normals.push_back(getNormalVector(phi_1, psi_2));
            normals.push_back(getNormalVector(phi_2, psi_1));
        }
    }
    DataBufferPtr points_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    points_buf->setData(3 * sizeof(float) * points.size(), points.data());

    DataBufferPtr normals_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normals_buf->setData(3 * sizeof(float) * normals.size(), normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, points_buf);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normals_buf);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(static_cast<GLuint>(points.size()));

    return mesh;
}
void Torus::makeScene()
{
    Application::makeScene();
    _cameraMover = std::make_shared<FreeCameraMover>();
    this->torus = makeTorus();
    this->shader = std::make_shared<ShaderProgram>("491ShenshinaData/shader.vert", "491ShenshinaData/shader.frag");

    this->grid_shader = std::make_shared<ShaderProgram>("491ShenshinaData/grid_shader.vert", "491ShenshinaData/shader.frag");
}

void Torus::update()
{
    double dt = glfwGetTime() - _oldTime;
    if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        this->segments_number = std::max(3, int(this->segments_number / 1.05 / sqrt(dt + 1)));
    }
    if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        this->segments_number = std::min(200, int(ceil(this->segments_number * 1.05 * sqrt(dt + 1))));
    }
    this->torus = makeTorus(this->R, this->r, static_cast<int>(this->segments_number));
    Application::update();
}

void Torus::draw()
{
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glPolygonOffset(1, 1);
    this->shader->use();
    this->shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    this->shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    this->shader->setMat4Uniform("modelMatrix", this->torus->modelMatrix());
    this->torus->draw();

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    this->grid_shader->use();
    this->grid_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    this->grid_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    this->grid_shader->setMat4Uniform("modelMatrix", this->torus->modelMatrix());
    this->torus->draw();

}


